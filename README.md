# secgraph

This is a modified version of SecGraph, available under MIT license from http://www.secgraph.gatech.edu/

## Q & A
* How to make a runnable jar file from the secGraph code?
1. Load the secGraph project into Eclipse.
2. In Eclipse, Run (on the menu bar) -> Run Configurations.
3. In the Run Configurations dialog, create a New launch configuration (the top
   left icon on the left panel). Then name the configuration as "jarConfig". In
   its Main tab, specify the Project as "secGraph" and the Main class as
   "commandLine.commandLineMode".
4. Close the Run Configuration dialog. Right click on the secGraph project (in
   the Package Explorer), then select Export. Then select Java -> Runnable JAR
   file -> Next.
5. In the Runnable JAR File Export dialog, select the "jarConfig" created in
   step 3 as the Launch configuration, specify a Export destination, then select
   the "Package required libraries into generated JAR". Click Finish.
