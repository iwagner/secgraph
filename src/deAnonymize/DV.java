/*
 * 
 * The MIT License (MIT)
 * Copyright (c) <year> <copyright holders>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/
package deAnonymize;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime; 

import common.Graph;
import common.MinWeightHungarianAlgorithm;
import common.Pair;
import common.Utilities;

public class DV
{
	// Modified by Yuchen
	private static String statFileName;
	private static String distFileName;
	private static BufferedWriter statWriter;
	private static DataOutputStream distOutput;
	private static int numOfMappings = 0;

	// Modification done.
	public static void main(String[] args) throws IOException
	{
		if (args.length != 8)
		{
			System.out.println("DistanceVector usage : g1 g2 seedFile bipartSize numToKeep outputFileName mode");
			System.exit(0);
		}
		runDV(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
	}

	// Written by Yuchen
	private static void preFile(String outputFile) throws IOException
	{
		File f = new File(outputFile);
		new File(f.getParent() + "/dist/").mkdirs();
		statFileName = f.getParent() + "/dist/" + f.getName().split("\\.")[0] + "_stat.txt";
		statWriter = new BufferedWriter(new FileWriter(statFileName));
		distFileName = f.getParent() + "/dist/" + f.getName().split("\\.")[0] + "_dist.dat";
		distOutput = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(distFileName)));
	}

	// Written by Yuchen
	private static void outputDist(int node, HashMap<Integer, Double> scores) throws IOException
	{
		double sum = 0.0;
		ArrayList<Integer> nonZeroKeyList = new ArrayList<>();
		for (int key : scores.keySet())
		{
			double value = scores.get(key);
			if (value != 0)
			{
				nonZeroKeyList.add(key);
				sum += value;
			}
		}

		if (sum == 0.0)
		{
			return;
		}

		numOfMappings++;

		distOutput.writeInt(node);
		distOutput.writeInt(nonZeroKeyList.size());
		for (Integer key : nonZeroKeyList)
		{
			distOutput.writeInt(key);
		}
		for (Integer key : nonZeroKeyList)
		{
			distOutput.writeDouble(scores.get(key) / sum);
		}
	}

	public static void runDV(String g1, String g2, String seedFile, String BipartSize, String NumToKeep, String theta,
			String out, String mode) throws IOException
	{
		// Modified by Yuchen
		preFile(out);
		// Modification done.
		HashMap<Integer, HashSet<Integer>> G1 = Graph.readUndirectedGraph(g1);
		HashMap<Integer, HashSet<Integer>> G2 = Graph.readUndirectedGraph(g2);
		HashMap<Integer, Integer> seeds = Graph.getSeeds(seedFile);
		int bipartSize = Integer.parseInt(BipartSize);
		BufferedWriter w = new BufferedWriter(new FileWriter(out));
		int numToKeep = Integer.parseInt(NumToKeep);
		double thetaValue = Double.parseDouble(theta);
		w.write("g2ToG1seeds : \n");
		for (int i : seeds.keySet())
		{
			w.write(i + " " + seeds.get(i) + "\n");
		}
		// Modified by Yuchen
		int seedSize = seeds.size();
		statWriter.write("anonymized graph size:" + G2.size() + "\n");
		statWriter.write("seed size:" + seedSize + "\n");
		// Modification done.
		HashMap<Integer, Integer> res = distanceVector(G1, G2, seeds, bipartSize, numToKeep, thetaValue, mode);
		int r = 0;
		for (int i : res.keySet())
		{
			if (i == res.get(i))
			{
				r++;
			}
			w.write(i + " " + res.get(i) + "\n");
		}
		w.write(out + " " + r + "/" + res.size() + " " + G1.size() + " " + G2.size() + "\n");
		w.flush();
		w.close();
		// Modified by Yuchen
		statWriter.write("number of mappings:" + numOfMappings + "\n");
		statWriter.write("acc:" + (r - seedSize) + "/" + (res.size() - seedSize) + "\n");
		statWriter.close();
		distOutput.close();
		// Modification done.
	}

	private static HashMap<Integer, Integer> distanceVector(HashMap<Integer, HashSet<Integer>> g1,
			HashMap<Integer, HashSet<Integer>> g2, HashMap<Integer, Integer> g1ToG2Mapping, int bipartSize,
			int numToKeep, double theta, String mode) throws IOException
	{
		HashMap<Integer, HashMap<Integer, Integer>> g2Distance = new HashMap<Integer, HashMap<Integer, Integer>>();
		HashMap<Integer, HashMap<Integer, Integer>> g1Distance = new HashMap<Integer, HashMap<Integer, Integer>>();

		HashSet<Integer> g2Landmark = new HashSet<Integer>();
		HashSet<Integer> g1Landmark = new HashSet<Integer>();
		LinkedList<Integer> matchedG1Nodes = new LinkedList<Integer>();
		LinkedList<Integer> matchedG2Nodes = new LinkedList<Integer>();
		int numLandmark = g1ToG2Mapping.keySet().size();

		for (int g1Node : g1ToG2Mapping.keySet())
		{
			int g2Node = g1ToG2Mapping.get(g1Node);
			g2Landmark.add(g1Node);
			g1Landmark.add(g2Node);
			matchedG1Nodes.add(g1Node);
			matchedG2Nodes.add(g2Node);
			g2Distance.put(g2Node, new HashMap<Integer, Integer>());
			g1Distance.put(g1Node, new HashMap<Integer, Integer>());
			span(g1, g1Node, g1Distance.get(g1Node));
			span(g2, g2Node, g2Distance.get(g2Node));
		}

		LinkedList<Integer> sortedG1Nodes = getNodesInDegreeSortedOrder(g1, g1Landmark);
		LinkedList<Integer> sortedG2Nodes = getNodesInDegreeSortedOrder(g2, g2Landmark);

		// IW: record distributions based on pairwise scores for the two full graphs
		float[][] distscore = new float[sortedG1Nodes.size()][sortedG2Nodes.size()];
		for (int g1Cand = 0; g1Cand < sortedG1Nodes.size(); g1Cand++)
		{
			Integer G1Node = sortedG1Nodes.get(g1Cand);
			HashMap<Integer, Double> dist = new HashMap<>();
			for (int g2Cand = 0; g2Cand < sortedG2Nodes.size(); g2Cand++)
			{
				distscore[g1Cand][g2Cand] = calcCosWeight(numLandmark, g1Distance, g2Distance, sortedG1Nodes.get(g1Cand),
						sortedG2Nodes.get(g2Cand));
				double scoreG2Node = distscore[g1Cand][g2Cand] * -1;
				dist.put(sortedG2Nodes.get(g2Cand), scoreG2Node);
			}
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");  
			LocalDateTime now = LocalDateTime.now();  
			System.out.println("[" + dtf.format(now) + "] loop1, g1: " + g1Cand + "/" + sortedG1Nodes.size());
			
			outputDist(G1Node, dist);
		}
		// end IW
		
		
		while (sortedG2Nodes.size() > 0 && sortedG1Nodes.size() > 0)
		{
			// IW: fillCandidate() removes the top bipartSize nodes from sortedG2Nodes (we set bipartSize to 100)
			// fix to avoid infinite loop in the last iteration when the two graphs are not equal sizes
			int fillSize = bipartSize;
			if (sortedG1Nodes.size() < bipartSize || sortedG2Nodes.size() < bipartSize) {
				fillSize = Math.min(sortedG2Nodes.size(), sortedG1Nodes.size());
			}
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");  
			LocalDateTime now = LocalDateTime.now();  
			System.out.println("[" + dtf.format(now) + "] loop2, g1 size: " + sortedG1Nodes.size() + ", g2 size: " + sortedG2Nodes.size());
			ArrayList<Integer> candidateG2 = fillCandidate(sortedG2Nodes, fillSize);
			ArrayList<Integer> candidateG1 = fillCandidate(sortedG1Nodes, fillSize);

			float[][] score = new float[candidateG2.size()][candidateG1.size()];
			float[][] matrix = new float[candidateG2.size()][candidateG1.size()];
			
			// IW: this loop produces the scores we use for the probability distribution.
			// If we run this loop over all nodes in the two graphs (sortedG2Nodes and sortedG1Nodes), then
			// distributions should be correct.
			for (int g2Cand = 0; g2Cand < candidateG2.size(); g2Cand++)
			{
				for (int g1Cand = 0; g1Cand < candidateG1.size(); g1Cand++)
				{
					score[g2Cand][g1Cand] = calcCosWeight(numLandmark, g1Distance, g2Distance, candidateG1.get(g1Cand),
							candidateG2.get(g2Cand));
					matrix[g2Cand][g1Cand] = score[g2Cand][g1Cand];
				}
			}
			
			MinWeightHungarianAlgorithm match = new MinWeightHungarianAlgorithm();
			int[][] result = match.computeAssignments(matrix);
			PriorityQueue<Pair<Pair<Integer, Integer>, Float>> queue = createNodePairToScoreQueue();
			
			for (int i = 0; i < result.length; i++)
			{
				if (result[i] == null)
				{
					break;
				}
				double scoreMapping = score[result[i][0]][result[i][1]] * -1;
				if (scoreMapping < theta)
				{
					continue;
				}
				int nodeG1 = candidateG1.get(result[i][1]);
				int nodeG2 = candidateG2.get(result[i][0]);
				queue.add(new Pair<Pair<Integer, Integer>, Float>(new Pair<Integer, Integer>(nodeG1, nodeG2),
						score[result[i][0]][result[i][1]]));
			}
			int count = 0;
			while (!queue.isEmpty())
			{
				Pair<Pair<Integer, Integer>, Float> here = queue.poll();
				if (count < numToKeep)
				{

					int G1Node = here.getKey().getKey();
					int idxG1Node = candidateG1.indexOf(G1Node);
//					HashMap<Integer, Double> dist = new HashMap<>();
					for (int i = 0; i < candidateG2.size(); i++)
					{

//						Integer G2Node = candidateG2.get(i);
						double scoreG2Node = score[i][idxG1Node] * -1;
						if (scoreG2Node == 0)
							continue;
//						dist.put(G2Node, scoreG2Node);
					}
//					outputDist(G1Node, dist);
					matchedG1Nodes.add(here.getKey().getKey());
					matchedG2Nodes.add(here.getKey().getValue());
				} else
				{
					returnUnmatchedNode(sortedG1Nodes, here.getKey().getKey(), mode);
					returnUnmatchedNode(sortedG2Nodes, here.getKey().getValue(), mode);
				}
				count++;
			}
		}
		HashMap<Integer, Integer> res = new HashMap<Integer, Integer>();
		for (int i = 0; i < matchedG1Nodes.size(); i++)
		{
			res.put(matchedG2Nodes.get(i), matchedG1Nodes.get(i));
		}
//		int right = 0;
//		for (int i = 0; i < matchedG1Nodes.size(); i++)
//		{
//			if (matchedG1Nodes.get(i).equals(matchedG2Nodes.get(i)))
//			{
//				right++;
//			}
//		}
		return res;
	}

	private static void returnUnmatchedNode(LinkedList<Integer> sortedG1Nodes, Integer node, String mode)
	{
		if (mode.equals("queue"))
		{
			sortedG1Nodes.addLast(node);
		} else
		{
			sortedG1Nodes.addFirst(node);
		}
	}

	private static ArrayList<Integer> fillCandidate(LinkedList<Integer> nodes, int bipartSize)
	{
		ArrayList<Integer> candidates = new ArrayList<>();
		for (int i = 0; i < bipartSize; i++)
		{
			candidates.add(nodes.poll());
			if (nodes.isEmpty())
			{
				break;
			}
		}
		return candidates;
	}

	private static PriorityQueue<Pair<Pair<Integer, Integer>, Float>> createNodePairToScoreQueue()
	{
		return new PriorityQueue<Pair<Pair<Integer, Integer>, Float>>(100 * 100,
				new Comparator<Pair<Pair<Integer, Integer>, Float>>()
				{
					@Override
					public int compare(Pair<Pair<Integer, Integer>, Float> o1, Pair<Pair<Integer, Integer>, Float> o2)
					{
						return Float.compare(o1.getValue(), o2.getValue());
					}
				});
	}

	private static float calcCosWeight(int numLandmark, HashMap<Integer, HashMap<Integer, Integer>> sDistance,
			HashMap<Integer, HashMap<Integer, Integer>> cDistance, int sCand, int cCand)
	{
		ArrayList<Integer> I = new ArrayList<Integer>();
		ArrayList<Integer> J = new ArrayList<Integer>();
		for (int k : sDistance.keySet())
		{
			int distance;
			if (sDistance.get(k).get(sCand) == null)
			{
				distance = Integer.MAX_VALUE;
			} else
			{
				distance = sDistance.get(k).get(sCand);
			}
			I.add(distance);
			if (cDistance.get(k).get(cCand) == null)
			{
				distance = Integer.MAX_VALUE;
			} else
			{
				distance = cDistance.get(k).get(cCand);
			}

			J.add(distance);
		}
		float score = (float) Utilities.cosSimInt(I, J);
		return -1 * score;
	}

	private static LinkedList<Integer> getNodesInDegreeSortedOrder(HashMap<Integer, HashSet<Integer>> s,
			HashSet<Integer> Landmark)
	{
		LinkedList<Integer> sortedNode = new LinkedList<Integer>();
		TreeMap<Integer, HashSet<Integer>> sortedSDegreeToNodes = Graph.getDegreeToNodeMap(s);
		for (int i : sortedSDegreeToNodes.descendingKeySet())
		{
			for (int j : sortedSDegreeToNodes.get(i))
			{
				if (Landmark.contains(j))
				{
					continue;
				}
				sortedNode.add(j);
			}
		}
		return sortedNode;
	}

	private static void span(HashMap<Integer, HashSet<Integer>> graph, int id, HashMap<Integer, Integer> distance)
	{
		LinkedList<Pair<Integer, Integer>> queue = new LinkedList<Pair<Integer, Integer>>();
		HashSet<Integer> visited = new HashSet<Integer>();
		Pair<Integer, Integer> start = new Pair<Integer, Integer>(id, 0);
		distance.put(id, 0);
		visited.add(id);
		queue.addLast(start);
		while (!queue.isEmpty())
		{
			Pair<Integer, Integer> here = queue.pollFirst();
			int dist = here.getValue() + 1;
			for (int i : graph.get(here.getKey()))
			{
				if (!visited.contains(i))
				{
					visited.add(i);
					distance.put(i, dist);
					queue.add(new Pair<Integer, Integer>(i, dist));
				}
			}
		}
	}
}
