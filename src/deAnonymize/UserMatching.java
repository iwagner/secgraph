/*
 * 
 * The MIT License (MIT)
 * Copyright (c) <year> <copyright holders>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
*/
package deAnonymize;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeMap;

import common.Graph;

public class UserMatching
{// Modified by Yuchen
	private static String statFileName;
	private static String distFileName;
	private static BufferedWriter statWriter;
	private static DataOutputStream distOutput;
	private static int numOfMappings = 0;

	// Modification done.
	public static void main(String[] args) throws IOException
	{
		if (args.length != 6)
		{
			System.out.println("UserMatching usage : g1 g2 seedFile numToTest theta outputFileName");
			System.exit(0);
		}
		runUM(args[0], args[1], args[2], args[3], args[4], args[5]);
	}

	// Written by Yuchen
	private static void preFile(String outputFile) throws IOException
	{
		File f = new File(outputFile);
		new File(f.getParent() + "/dist/").mkdirs();
		statFileName = f.getParent() + "/dist/" + f.getName().split("\\.")[0] + "_stat.txt";
		statWriter = new BufferedWriter(new FileWriter(statFileName));
		distFileName = f.getParent() + "/dist/" + f.getName().split("\\.")[0] + "_dist.dat";
		distOutput = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(distFileName)));
	}

	// Written by Yuchen
	private static void outputDist(int node, HashMap<Integer, Double> scores) throws IOException
	{
		double sum = 0.0;
		ArrayList<Integer> nonZeroKeyList = new ArrayList<>();
		for (int key : scores.keySet())
		{
			double value = scores.get(key);
			if (value != 0)
			{
				nonZeroKeyList.add(key);
				sum += value;
			}
		}

		if (sum == 0.0)
		{
			return;
		}

		numOfMappings++;

		distOutput.writeInt(node);
		distOutput.writeInt(nonZeroKeyList.size());
		for (Integer key : nonZeroKeyList)
		{
			distOutput.writeInt(key);
		}
		for (Integer key : nonZeroKeyList)
		{
			distOutput.writeDouble(scores.get(key) / sum);
		}
	}

	public static void runUM(String g1, String g2, String SeedFile, String NumToTest, String theta, String out)
			throws IOException
	{
		// Modified by Yuchen
		preFile(out);
		// Modification done.
		HashMap<Integer, HashSet<Integer>> G1 = Graph.readUndirectedGraph(g1);// Graph.sampleGraph(G, prob);
		HashMap<Integer, HashSet<Integer>> G2 = Graph.readUndirectedGraph(g2);
		HashMap<Integer, Integer> g1ToG2Mapping = Graph.getSeeds(SeedFile);// Graph.getSeeds(G1,Integer.parseInt(args[2]),Integer.parseInt(args[5]));
		// Modified by Yuchen
		int seedSize = g1ToG2Mapping.size();
		statWriter.write("anonymized graph size:" + G2.size() + "\n");
		statWriter.write("seed size:" + seedSize + "\n");
		// Modification done.
		HashMap<Integer, Integer> res = UserMatching.userMatching(G1, G2, g1ToG2Mapping, Integer.parseInt(theta),
				Integer.parseInt(NumToTest));
		BufferedWriter w = new BufferedWriter(new FileWriter(out));
		int r = 0;
		for (int i : res.keySet())
		{
			if (i == res.get(i))
			{
				r++;
			}
			w.write(i + " " + res.get(i) + "\n");
		}
		w.write(out + " " + r + "/" + res.size() + " " + G1.size() + " " + G2.size() + "\n");
		w.flush();
		w.close();
		// Modified by Yuchen
		statWriter.write("number of mappings:" + numOfMappings + "\n");
		statWriter.write("acc:" + (r - seedSize) + "/" + (res.size() - seedSize) + "\n");
		statWriter.close();
		distOutput.close();
		// Modification done.
	}

	public static HashMap<Integer, Integer> userMatching(HashMap<Integer, HashSet<Integer>> g1,
			HashMap<Integer, HashSet<Integer>> g2, HashMap<Integer, Integer> g1ToG2Mapping, int theta, int numToTest)
			throws IOException
	{
		// HashMap<Integer, Integer> g1DegreeMap = Graph.getGraphDegreeMap(g1);
		// HashMap<Integer, Integer> g2DegreeMap = Graph.getGraphDegreeMap(g2);
		HashSet<Integer> nodesInG1Matched = new HashSet<Integer>();
		HashSet<Integer> nodesInG2Matched = new HashSet<Integer>();
		for (int i : g1ToG2Mapping.keySet())
		{
			nodesInG1Matched.add(i);
			nodesInG2Matched.add(g1ToG2Mapping.get(i));
		}
		LinkedList<Integer> sortedG1Nodes = getNodesInDegreeSortedOrder(g1, nodesInG1Matched);
		LinkedList<Integer> sortedG2Nodes = getNodesInDegreeSortedOrder(g2, nodesInG2Matched);
		
		
		// IW: record distributions based on pairwise scores for the two full graphs
		HashMap<Integer, Double> dist = new HashMap<>();
		for (int g1cand = 0; g1cand < sortedG1Nodes.size(); g1cand++) {
			Integer G1Node = sortedG1Nodes.get(g1cand);
			for (int i = 0; i < sortedG2Nodes.size(); i++)
			{
				Integer G2Node = sortedG2Nodes.get(i);
				double scoreG2Node = findCommonNei(g1, g2, G1Node, G2Node, g1ToG2Mapping);
				dist.put(G2Node, scoreG2Node);
			}
			outputDist(G1Node, dist);
		}
		// end IW
		
		ArrayList<Integer> candidateG1 = new ArrayList<Integer>();
		ArrayList<Integer> candidateG2 = new ArrayList<Integer>();
		while (sortedG1Nodes.size() > 0 && sortedG2Nodes.size() > 0)
		{
			// IW: if branch gets taken if both graphs have more nodes than bipart_size 
			if (candidateG1.size() + sortedG1Nodes.size() > numToTest
					&& candidateG2.size() + sortedG2Nodes.size() > numToTest)
			{
				while (candidateG1.size() < numToTest)
				{
					candidateG1.add(sortedG1Nodes.pollFirst());
					candidateG2.add(sortedG2Nodes.pollFirst());
				}
				int largestG1Cand = -1, largestG2Cand = -1, largestScore = -1;
				for (int g1Cand : candidateG1)
				{
					for (int g2Cand : candidateG2)
					{
						int numCommonNei = findCommonNei(g1, g2, g1Cand, g2Cand, g1ToG2Mapping);
						// IW: record the two nodes that are linked by the most seed pairs
						if (numCommonNei > largestScore)
						{
							largestScore = numCommonNei;
							largestG1Cand = g1Cand;
							largestG2Cand = g2Cand;
						}
					}
				}
				// System.out.println(largestG1Cand+" "+largestG2Cand);
				// IW: we set theta to 1
				// IW: output distribution commented because it is now done above before the while loops 
				if (largestScore >= theta)
				{
//					HashMap<Integer, Double> dist = new HashMap<>();
					for (int i = 0; i < candidateG2.size(); i++)
					{

//						Integer G2Node = candidateG2.get(i);
						double scoreG2Node = findCommonNei(g1, g2, largestG1Cand, largestG2Cand, g1ToG2Mapping);
						if (scoreG2Node == 0)
							continue;
//						dist.put(G2Node, scoreG2Node);
					}
//					outputDist(largestG1Cand, dist);

					g1ToG2Mapping.put(largestG1Cand, largestG2Cand);
				}
				sortedG1Nodes.remove(new Integer(largestG1Cand));
				sortedG2Nodes.remove(new Integer(largestG2Cand));
				candidateG1.remove(new Integer(largestG1Cand));
				candidateG2.remove(new Integer(largestG2Cand));
			// IW: else branch gets taken when at least one graph has less than bipart_size nodes left to match
			} else
			{
				for (int i = 0; i < sortedG1Nodes.size(); i++)
				{
					candidateG1.add(sortedG1Nodes.pollFirst());
					candidateG2.add(sortedG2Nodes.pollFirst());
				}
				
				// IW: remove null nodes from candidateG2 list 
				ArrayList<Integer> to_remove = new ArrayList<Integer>();
				for (Integer g2Cand : candidateG2)
				{
					if (g2Cand == null)
					{
						to_remove.add(g2Cand);
					}
				}
				for (Integer g2Cand : to_remove)
				{
					candidateG2.remove(g2Cand);
				}
				
				while (candidateG1.size() > 0)
				{
					int largestG1Cand = -1, largestG2Cand = -1, largestScore = -1;
					for (int g1Cand : candidateG1)
					{
					for (Integer g2Cand : candidateG2)
						{
							if (g2Cand != null)
							{
								int g2Cand_int = g2Cand;
								int numCommonNei = findCommonNei(g1, g2, g1Cand, g2Cand_int, g1ToG2Mapping);
								if (numCommonNei > largestScore)
								{
									largestScore = numCommonNei;
									largestG1Cand = g1Cand;
									largestG2Cand = g2Cand_int;
								}
							}
						}
					}
					if (largestScore >= theta)
					{
//						HashMap<Integer, Double> dist = new HashMap<>();
//						for (int i = 0; i < candidateG2.size(); i++)
//						{
//							Integer G2Node = candidateG2.get(i);
//							double scoreG2Node = findCommonNei(g1, g2, largestG1Cand, largestG2Cand, g1ToG2Mapping);
//							if (scoreG2Node == 0)
//								continue;
//							dist.put(G2Node, scoreG2Node);
//						}
//						outputDist(largestG1Cand, dist);
						g1ToG2Mapping.put(largestG1Cand, largestG2Cand);
					}
					sortedG1Nodes.remove(new Integer(largestG1Cand));
					sortedG2Nodes.remove(new Integer(largestG2Cand));
					candidateG1.remove(new Integer(largestG1Cand));
					candidateG2.remove(new Integer(largestG2Cand));
					// IW: stop while loop when candidateG2 list is empty 
					if (candidateG2.size() == 0)
					{
						break;
					}
				}
			}
		}

		/*
		 * int numRight = 0; for(int a : g1ToG2Mapping.keySet()){
		 * if(g1ToG2Mapping.get(a)==a){ numRight++; }
		 * System.out.println(a+" "+g1ToG2Mapping.get(a)); }
		 * System.out.println(numRight+"/"+g1ToG2Mapping.size()+"mapped correctly");
		 */
		return g1ToG2Mapping;
	}

	private static int findCommonNei(HashMap<Integer, HashSet<Integer>> g1, HashMap<Integer, HashSet<Integer>> g2,
			int g1Cand, int g2Cand, HashMap<Integer, Integer> g1ToG2Mapping)
	{
		// IW: returns the number of seed pairs that link g1Cand and g2Cand
		int commonNei = 0;
		HashSet<Integer> g2Nei = g2.get(g2Cand);
		for (int i : g1.get(g1Cand))
		{
			if (g1ToG2Mapping.containsKey(i) && g2Nei.contains(g1ToG2Mapping.get(i)))
			{
				commonNei++;
			}
		}
		return commonNei;
	}

	/*
	 * private static PriorityQueue<Pair<Pair<Integer, Integer>, Integer>>
	 * createNodePairToScoreQueue() { return new
	 * PriorityQueue<Pair<Pair<Integer,Integer>,Integer>>(100*100, new
	 * Comparator<Pair<Pair<Integer,Integer>,Integer>>(){
	 * 
	 * @Override public int compare(Pair<Pair<Integer,Integer>, Integer> o1,
	 * Pair<Pair<Integer,Integer>, Integer> o2) { return
	 * -1*Integer.compare(o1.getValue(), o2.getValue()); } }); }
	 */
	private static LinkedList<Integer> getNodesInDegreeSortedOrder(HashMap<Integer, HashSet<Integer>> s,
			HashSet<Integer> Landmark)
	{
		LinkedList<Integer> sortedNode = new LinkedList<Integer>();
		TreeMap<Integer, HashSet<Integer>> sortedSDegreeToNodes = Graph.getDegreeToNodeMap(s);
		for (int i : sortedSDegreeToNodes.descendingKeySet())
		{
			for (int j : sortedSDegreeToNodes.get(i))
			{
				if (Landmark.contains(j))
				{
					continue;
				}
				sortedNode.add(j);
			}
		}
		return sortedNode;
	}
}
