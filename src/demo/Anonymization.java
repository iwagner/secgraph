package demo;

import java.io.IOException;

import org.apache.commons.cli.ParseException;

import commandLine.commandLineMode;

public class Anonymization {

	public static void main(String[] args) throws ParseException, IOException {
		// Demo for ID Removal.
		String[] cmdIdRemoval = { "-m", "a", "-a", "id", "-gA", "data/input/edges.txt", "-gO",
				"data/output/a_idremoval.txt" };
		// commandLineMode.main(cmdIdRemoval);

		// Demo for Add Del.
		String[] cmdAddDel = { "-m", "a", "-a", "addDel", "-gA", "data/input/edges.txt", "-gO",
				"data/output/a_adddel.txt", "-f", "0.10" };
		// commandLineMode.main(cmdAddDel);

		// Demo for Switch.
		String[] cmdSwitch = { "-m", "a", "-a", "sw", "-gA", "data/input/edges.txt", "-gO", "data/output/a_switch.txt",
				"-f", "0.10" };
		// commandLineMode.main(cmdSwitch);

		// Demo for k-Degree Anonymity.
		String[] cmdKDA = { "-m", "a", "-a", "kDa", "-gA", "data/input/edges.txt", "-gO", "data/output/a_kda.txt", "-k",
				"3" };
		// commandLineMode.main(cmdKDA);

		// Demo for k-Isomorphism.
		String[] cmdKISO = { "-m", "a", "-a", "kIso", "-gA", "data/input/edges.txt", "-k", "3", "-curNNum", "50000",
				"-statusResult", "data/output/a_kiso_status.txt", "-edgeResult", "data/output/a_kiso_edge.txt" };
		// commandLineMode.main(cmdKISO);

		// Demo for t Mean.
		String[] cmdTMEAN = { "-m", "a", "-a", "tMean", "-gA", "data/input/edges.txt", "-gO", "data/output/a_tmean.txt",
				"-t", "3" };
		// commandLineMode.main(cmdTMEAN);

		// Demo for Differential Privacy by Sala et al., i.e. Pygmalion.
		// The epsilon must be a integer?!
		String[] cmdSalaDP = { "-m", "a", "-a", "salaDP", "-gA", "data/input/edges.txt", "-gO",
				"data/output/a_saladp.txt", "-e", "1" };
		// commandLineMode.main(cmdSalaDP);

		// Demo for Differential Privacy by Xiao et al., i.e. Hierarchical Random Graph
		// (HRG).
		// Does not work properly right now. Cannot find the output.
		String[] cmdXiaoDP = { "-m", "a", "-a", "XiaoDP", "-gA", "data/input/edges.txt" };
		// commandLineMode.main(cmdXiaoDP);

		// Demo for Random Walk Anonymization.
		String[] cmdRWalk = { "-m", "a", "-a", "rWalk", "-gA", "data/input/edges.txt", "-gO", "data/output/a_rwalk.txt",
				"-rn", "3", "-rm", "3" };
//		commandLineMode.main(cmdRWalk);

		// Demo for Union Split.
		// Does not work properly right now. OutOfMemoryError. The size of each cluser is too big (38000).
		String[] cmdUnion = { "-m", "a", "-a", "union", "-gA", "data/input/edges.txt", "-gO", "data/output/a_union.txt",
				"-k", "10000" };
//		commandLineMode.main(cmdUnion);
	}

}
